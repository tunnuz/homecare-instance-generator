#!/usr/bin/perl
use strict;
use warnings;

my $a;
my $oc;
my $h;
my $s;
my $out;
my $cmd;

foreach((20, 30, 40)) {
    $a = $_;
    foreach((0.5, 0.6, 0.7))
    {
        $oc = $_;
        foreach((3, 6))
        {
            $h = $_;
            for($s = 0; $s < 5; $s++){
                $out = "instances/udine_$a-$oc-$h-$s.json";
                $cmd = "./gen.py -i config/udine.json -o $out -a $a -oc $oc -ho $h -r $s";
                print $cmd, "\n";
                `$cmd`;
            }    
        }
    }
}