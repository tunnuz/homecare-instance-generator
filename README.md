# Random instance generator for HomeCare

Instance generator for Home Care scheduling problems. The generator is highly customizable, but it doesn't guarantees the feasibility of the generated instances.

## Parameters

The generator can be fed a configuration file, written in JSON one example is `config/udine.json`. The configuration file overwrites any subset of the default settings, which are described by the following "implicit" configuration.

```
{
    "random_seed": 0,
    "patient_correction": 1.0,
    "operator_correction": 1.0,
    "operator_gamma_alpha": 1,
    "operator_gamma_beta": 0.5,
    "operator_max_daily_hours": 6,
    "operator_incompatibility_rate": 0.2,
    "operator_unavailable_days_rate": 0.01,
    "operator_shifts": {
        "1": 0.7,
        "2": 0.3
    },
    "operator_regular_working_rate": 0.8,
    "max_overtime_rate": 1.1,
    "activities": 20,
    "multiple_activity_ratio": 0.2,
    "activity_duration": {
        "3": 0.6,
        "6": 0.4
    },
    "activity_shifts": {
        "1": 0.3,
        "2": 0.4,
        "3": 0.3
    },
    "activity_precedence_rate":
    "horizon": 6,
    "max_consecutive_days": 5,
    "granularity": 10,
    "time_window": {
        "start": "08:00:00",
        "finish": "18:00:00"
    },
    "start_date": datetime.today().isoformat(),
    "upper_left":   [ 46.101566, 13.160539 ],
    "lower_right":  [ 46.040591, 13.301988 ]
}
```

Following is a rundown of the meaning of the various parameters:

* **random seed** seed for the pseudo-random number generators (for repeatability),
* **granularity** minimum considered unit of time (all the time quantities are expressed with this granularity, e.g., if the granularity is 10, then 6 time units are one hour,
* **horizon** length of the planning horizon, e.g., 5 days,
* **time window** the length of the daily time window, e.g., from 8 ("start") to 18 ("finish"),
* **activities** how many activities are generated every day,
* **multiple activity ratio** percentage of activities that have multiple operator requirements,
* **activity duration** available activity durations (explicit),
* **activity shifts** percentages of activities with a given time window, for instance
    * `"1": 0.3` means that 30% of the activities have a time window which is as broad as the whole day (here "1" stands for "daily time window/1"),
    * `"2": 0.4` means that 40% of the activities have a time window which is half a day broad (here "2" stands for "daily time window/2"),
    * so forth ... the sum of the percentages has to be 1,
* **start date** the date in which the planning horizon starts, in ISO format,
* **upper left** upper left corner of the generation area,
* **lower right** lower right corner of the generation area,
* **operator correction** the number of available operators is calculated heuristically, in order to make sure that all the activities can be carried out; since this number if often too conservative, it can be multiplied by the operator correction ratio to make instances harder to solve,
* **operator gamma alpha** the alpha parameter for the gamma distribution from which the number of operators is drawn, in case of a multi-operator activity; the expected number of needed operators is 1+alpha,
* **operator gamma alpha** the beta parameter for the above gamma distribution, it controls how fast the probability of getting more operators on a multi-operator activity decreases,
* **operator max daily hours** maximum number of working hours per day for an operator,
* **operator shifts** same as **activity shifts** but for operators,
* **operator regular working rate** amount of the full time window which counts as "regular worktime" for an operator,
* **max overtime rate** maximum amount of overtime (over the full time windows) which is allowed without penalty,
* **operator incompatibility rate** probability of incompatibility between an activity and an operator,
* **operator unavailable days rate** probability of unavailability of operator on a given day,
* **patient correction** number that, multiplied by the number of activities, gives the number of total patients, this is needed to generate activities with precedence constraints between them (it only makes sense to have precedences between activities of the same patient, and activities in the same shift),
* **activity precedence rate** probability of having a precedence constraint between two activities with the same patient and same shift.


Some of the parameters can be also overridden by passing specific command line parameters to the generator, specifically **activities**, **horizon**, **operator correction**, and **random seed**. If both a configuration file and command line options are passed, the command line options are prioritary.
