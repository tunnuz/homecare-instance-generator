#!/usr/bin/python
import names
import json
from datetime import datetime, timedelta
import iso8601
import time
from random import seed, gammavariate, uniform, sample, shuffle
from math import ceil, floor
from argparse import ArgumentParser as parser, ArgumentDefaultsHelpFormatter as formatter


def main():
    """Main (entry point)"""

    # setup arguments
    p = parser(formatter_class=formatter)
    setup_arguments(p)

    # parse arguments
    args = p.parse_args()

    # read input, update default parameters
    try:
        i = open(args.input)
        input = json.load(i)
        settings.update(input)
        i.close()

    except IOError as e:
        print e
        exit(1)

    # update parameters with content of args
    if args.activities:
        settings["activities"] = int(args.activities)

    if args.horizon:
        settings["horizon"] = int(args.horizon)

    if args.operator_correction:
        settings["operator_correction"] = float(args.operator_correction)

    if args.random_seed:
        settings["random_seed"] = int(args.random_seed)

    # seed random generator
    seed(settings["random_seed"])

    # coerce to right types (just in case the input file wasn't written correctly)
    settings["activity_duration"] = { int(ad): float(settings["activity_duration"][str(ad)]) for ad in settings["activity_duration"] }
    settings["activity_shifts"] = { int(ad): float(settings["activity_shifts"][str(ad)]) for ad in settings["activity_shifts"] }
    settings["operator_shifts"] = { int(ad): float(settings["operator_shifts"][str(ad)]) for ad in settings["operator_shifts"] }

    # if max_consecutive_days is negative then it is (horizon+max_consecutive_days)
    if settings["max_consecutive_days"] < 0:
        settings["max_consecutive_days"] = settings["horizon"] + settings["max_consecutive_days"]

    # time windows (as time)
    tw_start = time.strptime(settings["time_window"]["start"], "%H:%M:%S")
    tw_finish = time.strptime(settings["time_window"]["finish"], "%H:%M:%S")

    # base dates
    dt_start = iso8601.parse_date(settings["start_date"])
    dt_start = dt_start.replace(hour=0, minute=0, second=0, microsecond=0, tzinfo=None)
    dt_finish = dt_start + timedelta(days=settings["horizon"])

    # time windows (as date, first day, for operations)
    tw_start = dt_start.replace(hour=tw_start.tm_hour, minute=tw_start.tm_min, second=0, microsecond=0);
    tw_finish = dt_start.replace(hour=tw_finish.tm_hour, minute=tw_finish.tm_min, second=0, microsecond=0);

    # number of activities per shift type (heuristic, for number of operators)
    n_act_shift = { s: int(ceil((settings["activities"] * settings["activity_shifts"][s])/s)) for s in settings["activity_shifts"] }

    # number of operators (heuristic + correction)
    ops = int(ceil(sum(n_act_shift.values()) * settings["operator_correction"]))

    # expanded activity shift (with probabilities)
    exp_act_shifts = { }
    exp_op_shifts = { }

    for i in settings["activity_shifts"]:

        shift_length = (tw_finish - tw_start).seconds / i
        shift_length_min = shift_length / 60

        # split time window into shifts
        exp_act_shifts.update({
            ((tw_start + timedelta(seconds=j*shift_length)).isoformat(), (tw_start + timedelta(seconds=(j+1)*shift_length)).isoformat()): (settings["activity_shifts"][i] * 100) / (i * 100)
        for j in xrange(i) })

    # expanded operator shift (with probabilities)
    for i in settings["operator_shifts"]:

        shift_length = (tw_finish - tw_start).seconds / i
        shift_length_min = shift_length / 60

        # split time window into shifts
        exp_op_shifts.update({
            ((tw_start + timedelta(seconds=j*shift_length)).isoformat(), (tw_start + timedelta(seconds=(j+1)*shift_length)).isoformat()): (settings["operator_shifts"][i] * 100) / (i * 100)
        for j in xrange(i) })

    # number of incompatible activities
    inc_act = int(ceil(settings["activities"] * settings["operator_incompatibility_rate"]))

    # generate patients
    patients = [ names.get_full_name() for p in range(int(floor(settings["patient_correction"] * settings["activities"]))) ]

    # instance generation
    instance = {
        "meta": {
            "horizon": settings["horizon"],
            "granularity": settings["granularity"],
            "time_window": settings["time_window"],
            "start_date": dt_start.isoformat(),
            "finish_date": dt_finish.isoformat(),
            "max_consecutive_days": settings["max_consecutive_days"],
            "max_overtime_rate": settings["max_overtime_rate"],
            "geographic_area": {
              "lower_right": settings["lower_right"],
              "upper_left" : settings["upper_left"]
            }
        },
        "activities": [ ]
    }
    all_act = []

    # treat days separately
    for d in range(settings["horizon"]):

        # generate locations based on number of activities
        activities = {
            "act_%d_%d" % (d, a) : {
                #"ordinal": d * settings["activities"] + a,
                "patient_name": str(sample(patients,1)),
                "time_window": to_date_window(select_with_probability(exp_act_shifts),d),
                "duration": select_with_probability(settings["activity_duration"]) * settings["granularity"],
                "required_operators": operators(),
                "location": {
                    "latitude": uniform(settings["upper_left"][0], settings["lower_right"][0]),
                    "longitude": uniform(settings["upper_left"][1], settings["lower_right"][1])
                },
                "precedences": {
                    "within": [],
                    "since": [],
                    ""
                }
                #,
                #"incompatible_operators": sample(range(ops), int(settings["operator_incompatibility_rate"] * ops))
            }
        for a in range(settings["activities"]) }

        # add precedence constraints to activities
        for shift in exp_act_shifts:
            acts_in_shift = [ activities[act] for act in activities if  activities[act]["time_window"] == to_date_window(shift,d)]

            # shuffle lists of actions, add consistent precedence constraints
            acts_in_shift.shuffle()

            # identify double occurrences of patients in the same shift
            patient_acts = {}
            for act in acts_in_shift:
                patient = act["patient_name"]
                if patient in patient_acts:
                    patient_acts[patient].append(act)
                else:
                    patient_acts[patient] = [ act ]
            patient_acts = { p: patient_acts[p] for p in patient_acts if len(patient_acts[p]) > 1 }

            # shuffle lists of actions, add consistent precedence constraints
            for patient in patient_acts:
                for a1 in range(len(patient_acts[patient])):
                    for a2 in range(a1, len(patient_acts[patient])):




        instance["activities"].append(activities)

        # add to list of all activities (for incompatibilities)
        all_act.extend(activities.keys())

    # operators in instance
    instance["operators"] = {
        "op_%d" % o: {
            #"ordinal": o,
            "name": str(names.get_full_name()),
            "incompatible_activities": sample(all_act, inc_act)
        }
    for o in range(ops) }

    # expand operator time windows
    for o in instance["operators"].values():
        tw = []
        for d in range(settings["horizon"]):
            if uniform(0,1) > settings["operator_unavailable_days_rate"]:
                start, finish = select_with_probability(exp_op_shifts)
                wd = (iso8601.parse_date(finish) - iso8601.parse_date(start)).total_seconds()
                tw.append({
                    "start": fix_date(start, d),
                    "finish": fix_date(finish, d),
                    "regular_worktime":  str(timedelta(seconds = wd * settings["operator_regular_working_rate"]))
                })
            else:
                tw.append({})

        o["time_windows"] = [dict(t) for t in tw]
        # compute the lower and upper bound on the total working hours in the horizon
        # FIXME: currently the measure is really raw and it works correctly with a 1 week horizon
        ordered_tw = [t for t in tw if len(t) > 0]
        ordered_tw = sorted(ordered_tw, key = lambda t: iso8601.parse_date(t["finish"]) - iso8601.parse_date(t["start"]))
        map(lambda t: t.update(worktime = (iso8601.parse_date(t["finish"]) - iso8601.parse_date(t["start"])).total_seconds() * settings["operator_regular_working_rate"]), ordered_tw)
        groups = ceil(float(settings["max_consecutive_days"]) / float(settings["horizon"]))
        d = int(groups * int(settings["max_consecutive_days"]))
        min_wh = reduce(lambda total, t: total + t["worktime"], ordered_tw[0:d], 0)
        max_wh = reduce(lambda total, t: total + t["worktime"], ordered_tw[-d:], 0)

        o["minimum_working_hours"] = "%02d:00:00" % (min_wh / 3600)
        o["maximum_working_hours"] = "%02d:00:00" % (max_wh / 3600)



    # output
    if args.output:
        o = open(args.output, "w")
        json.dump(instance, o, indent=4)
        o.close()
    else:
        print json.dumps(instance, indent=4)

def setup_arguments(p):
    """Setups command line parser arguments"""

    # mandatory parameter: a JSON file describing the instance to be generated
    p.add_argument("--input", "-i", required = True, type = str, help="input file (JSON, see test.json)")

    # auxiliary parameters (will override those specified in the JSON if set)
    p.add_argument("--random-seed", "-r", required = False, type = int, help="random seed for the pseudo-random number generator")
    p.add_argument("--horizon", "-ho", required = False, type = int, help="instance horizon (days)")
    p.add_argument("--activities", "-a", required = False, type = int, help="number of activities to generate")
    p.add_argument("--operator-correction", "-oc", required = False, type = float, help="operator correction")

    # output
    p.add_argument("--output", "-o", required=False, type=str, help="output (JSON) file")


def operators_dist(alpha, beta):
    """Probabilities for numbers of operators given alpha and beta"""
    a = {}
    for i in xrange(100000):
    	x = max(1+alpha, 1 + math.ceil(random.gammavariate(alpha, beta)))
    	if x in a:
    		a[x] += 1
    	else:
    		a[x] = 1
    return {k: a[k] / 100000.0 for k in a}

def operators():
    """Number of operators (overall)"""
    if (uniform(0,1) < settings["multiple_activity_ratio"]):
        return int(multiple_operators(settings["operator_gamma_alpha"],settings["operator_gamma_beta"]))
    else:
        return int(1)

def multiple_operators(alpha, beta):
    """Number of operators, given alpha and beta"""
    return max(1+alpha, 1 + ceil(gammavariate(alpha,beta)))

def to_date_window(x,d):
    """Transform tuple in dictionary formatted as time window with date"""
    return { "start": fix_date(x[0],d), "finish": fix_date(x[1],d) }

def to_time_window(x):
    """Transform tuple in dictionary formatted as time window"""
    return { "start": x[0], "finish": x[1] }

def select_with_probability(probs):
    """Takes dictionary of probabilities, return key stochastically based on value"""

    r = uniform(0,sum(probs.values()))
    pk = probs.keys()
    pv = probs.values()

    i = 0
    while r > pv[i]:
        r -= pv[i]
        i+=1

    return pk[i]

def fix_date(date, d):
    """Takes a date plus a days offset"""
    dt = iso8601.parse_date(date)
    dt += timedelta(days=d)
    dt = dt.replace(tzinfo=None)
    return dt.isoformat()

# default settings
settings = {

    # repeatability
    "random_seed": 0,                           # seed for all random generators

    # patient-related
    "patient_correction": 0.5,                  # number that, multiplied by the number of activities, gives the number of patients

    # operator-related
    "operator_correction": 1.0,                 # operators = computed heuristically
    "operator_gamma_alpha": 1,                  # activities with multiple ops will have expectedly 1+alpha operators (more with decreasing prob.)
    "operator_gamma_beta": 0.5,                 # parameter to tune how slow the probability of getting more than 1+alpha operators decreases
    "operator_max_daily_hours": 6,              # maximum number of working hours per day
    "operator_incompatibility_rate": 0.2,       # probability of incompatible operator-activity
    "operator_unavailable_days_rate": 0.01,     # probability of unavailability of the operator for a single day (independent from other days)
    "operator_shifts": {
        "1": 0.7,                               # operators with day-long time window ...
        "2": 0.3
    },
    "operator_regular_working_rate": 0.8,       # amount of the full time-window which is regular worktime allowed per day
    "max_overtime_rate": 1.1,                   # amount of total maximum overtime allowed in a week (multiplier of the total regular worktime)

    # activity-related
    "activities": 20,                           # how many activities in total
    "multiple_activity_ratio": 0.2,             # how many activities require multiple operators
    "activity_duration": {
        "3": 0.6,                               # activities of 30 minutes: 60%
        "6": 0.4                                # activities of 60 minutes: 40%
    },
    "activity_shifts": {
        "1": 0.3,                               # activities with day-long time window
        "2": 0.4,                               # activities with half-a-day-long time window
        "3": 0.3                                # activities with 3-a-day-long time window ...
    },
    "activity_precedence_rate": 0.5             # probability of having a precedence constraint between two activities with the same patient and same shift
    # time-related
    "horizon": 6,                               # 6 days (a week)
    "max_consecutive_days": 5,                  # maximum number of consecutive workdays
    "granularity": 10,                          # 10 minutes
    "time_window": {
        "start": "08:00:00",                    # parsed with strptime(s, "%H:%M")
        "finish": "18:00:00"                    # parsed with strptime(s, "%H:%M")
    },
    "start_date": datetime.today().isoformat(),

    # geolocation-related
    "upper_left":   [ 46.101566, 13.160539 ],   # udine
    "lower_right":  [ 46.040591, 13.301988 ]
}

if __name__ == "__main__":
    main()
